package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public interface CompanyService {

    List<Company> companyList = new ArrayList<>(Arrays.asList(
            new Company(1 ,"spring"),
            new Company(2 ,"summer"),
            new Company(3 ,"autumn"),
            new Company(4 ,"winter")

    ));

    List<Company> getAllCompanies();

    Company getCompanyById(Integer id);

    List<Employee> getEmployeesById(Integer id);

    List<Company> getCompanyByPage(Integer page, Integer size);

    Company addCompany(Company company);

    Company modifyCompany(Company company, Integer id);

    void removeCompany(Integer id);
}
