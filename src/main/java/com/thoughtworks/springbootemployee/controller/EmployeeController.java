package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/employees")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping
    public List<Employee> getEmployees() {
        return employeeService.getEmployees();
    }
    @GetMapping(path = "/{id}")
    public Employee getEmployeeById(@PathVariable Integer id) {
        return employeeService.getEmployeeById(id);
    }
    @GetMapping(params = "sex")
    public List<Employee> getEmployeeBySex(@RequestParam String sex) {
        if(sex == null) return null;
        return employeeService.getEmployeeBySex(sex);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Employee addEmployee(@RequestBody Employee employee){
        return employeeService.addEmployee(employee);
    }

    @PutMapping("/{id}")
    public Employee modifyEmployee(@PathVariable Integer id, @RequestBody Employee employee){
        return employeeService.modifyEmployee(employee,id);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployee(@PathVariable Integer id){
        Employee dbEmployee = getEmployeeById(id);
        employeeService.employees.remove(dbEmployee);
    }

    @GetMapping(params = {"page", "size"})
    public List<Employee> pageEmployee(@RequestParam Integer page, @RequestParam Integer size){
        return employeeService.getEmployees().subList((page - 1) * size, (page - 1) * size + size);
    }

}
