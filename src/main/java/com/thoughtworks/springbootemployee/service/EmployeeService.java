package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.entity.Employee;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public interface EmployeeService {

    ArrayList<Employee> employees = new ArrayList<>(Arrays.asList(
            new Employee(1, "Charlie", "male", 20, 100000,1),
            new Employee(2, "Garrick", "male", 20, 100000,1))
    );

    Employee getEmployeeById(Integer id) ;

    List<Employee> getEmployees();

    List<Employee> getEmployeeBySex(String sex);

    List<Employee> getEmployeesByCompanyId(Integer id);

    Employee modifyEmployee(Employee employee, Integer id);

    Employee addEmployee(Employee employee);
}
