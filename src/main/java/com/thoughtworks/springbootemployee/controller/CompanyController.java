package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/companies")
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    @GetMapping()
    public List<Company> getCompanies(){
        return companyService.getAllCompanies();
    }

    @GetMapping("/{id}")
    public Company getCompanyById(@PathVariable Integer id){
        return companyService.getCompanyById(id);
    }

    @GetMapping("/{id}/employees")
    public List<Employee> getEmployeesByCompany(@PathVariable Integer id){
        return companyService.getEmployeesById(id);
    }
    @GetMapping(params = {"page","size"})
    public List<Company> getCompaniesByPage(@RequestParam Integer page, @RequestParam Integer size){
        return companyService.getCompanyByPage(page,size);
    }
    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public Company addCompany(@RequestBody Company company){
        return companyService.addCompany(company);
    }
    @PutMapping("/{id}")
    public Company modifyCompany(@PathVariable Integer id, @RequestBody Company company){
        return companyService.modifyCompany(company,id);
    }
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
        public void removeCompany(@PathVariable Integer id){
        companyService.removeCompany(id);
    }
}
