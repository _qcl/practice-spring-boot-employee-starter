package com.thoughtworks.springbootemployee.service.impl;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.service.CompanyService;
import com.thoughtworks.springbootemployee.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    private EmployeeService employeeService;

    @Override
    public List<Company> getAllCompanies() {
        return companyList;
    }

    @Override
    public Company getCompanyById(Integer id) {
        return companyList.stream().filter(company -> company.getId() == id).findFirst().orElse(null);
    }

    @Override
    public List<Employee> getEmployeesById(Integer id) {
        return employeeService.getEmployeesByCompanyId(id);
    }

    @Override
    public List<Company> getCompanyByPage(Integer page, Integer size) {
        return companyList.subList((page - 1) * size,(page - 1) * size + size);
    }

    @Override
    public Company addCompany(Company company) {
        Integer id = companyList
                .stream()
                .max(Comparator.comparing(Company::getId))
                .orElse(new Company(0,null))
                .getId() + 1;
        company.setId(id);
        companyList.add(company);
        return company;
    }

    @Override
    public Company modifyCompany(Company company,Integer id) {
        getCompanyById(id).setName(company.getName());
        return company;
    }

    @Override
    public void removeCompany(Integer id) {
        companyList.removeIf(company -> company.getId().equals(id));
    }
}
