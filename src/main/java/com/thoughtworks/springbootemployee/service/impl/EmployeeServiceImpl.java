package com.thoughtworks.springbootemployee.service.impl;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.service.EmployeeService;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Override
    public Employee getEmployeeById(Integer id) {
        Employee dbEmployee = employees.stream().filter(employee -> employee.getId() == id).findFirst().get();
        return dbEmployee;
    }

    @Override
    public List<Employee> getEmployees() {
        return employees;
    }

    @Override
    public List<Employee> getEmployeeBySex(String sex) {
        return employees.stream().filter(employee -> sex.equals(employee.getSex())).collect(Collectors.toList());
    }

    @Override
    public List<Employee> getEmployeesByCompanyId(Integer id) {
        return employees.stream().filter(employee -> employee.getCompanyId() == id).collect(Collectors.toList());
    }

    @Override
    public Employee modifyEmployee(Employee employee, Integer id) {
        Employee employeeById = getEmployeeById(id);
        employeeById.setAge(employee.getAge());
        employeeById.setSalary(employee.getSalary());
        return employeeById;
    }

    @Override
    public Employee addEmployee(Employee employee) {
        Integer id = employees
                .stream()
                .max(Comparator.comparing(Employee::getId))
                .orElse(new Employee(0,null,null,0,0))
                .getId() + 1;
        employee.setId(id);
        employees.add(employee);
        return employee;
    }
}
